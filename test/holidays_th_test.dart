import 'package:flutter_test/flutter_test.dart';

import 'package:holidays_th/holidays_th.dart';

void main() {
  test('today must always be holiday', () {
    expect(isTodayHoliday(), false);
  });
}
